package usuario;

import java.util.Scanner;
import temperaturas.*;

public class main {


	public static void main(String[] args) {
		Scanner leitorEntrada = new Scanner(System.in);
		
		
		System.out.println("Qual temeperatura está usando?");
		System.out.println("1-Celsius");
		System.out.println("2-Fahrenheit");
		System.out.println("3- kelvin");
		
		Integer opcaoLida = leitorEntrada.nextInt();
		
		System.out.println("Digite a temperatura: ");
	//Double valorTemperatura = Double.parseDouble(leitorEntrada.next());
		Double valorTemperatura = leitorEntrada.nextDouble();
		Temperatura umaTemperatura = null;
		
		if(opcaoLida == 1){
			umaTemperatura = new Celsius();
		}
		else if (opcaoLida == 2){
			umaTemperatura = new Fahrenheit();
		}
		else if (opcaoLida == 3){
			umaTemperatura = new Kelvin();
		}
		String saida = umaTemperatura.converter(valorTemperatura);
		
		System.out.println(saida);
	}

}
