package temperaturas;

public class Celsius extends Temperatura{
	public String converter(Double temperatura){
		
		Double valorFahrenheit = (temperatura*1.8) + 32;
		Double valorKelvin = temperatura+273.15;
		
		return "Fahrenheit = " + valorFahrenheit + " valor Kelvin = " + valorKelvin;
	}
}
