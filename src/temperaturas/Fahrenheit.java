package temperaturas;

public class Fahrenheit extends Temperatura{

	public String converter(Double temperatura){
		Double valorCelsius = (temperatura-32)/1.8;
		Double valorKelvin = (temperatura+459.67)/1.8;
		return "Celsius = " + valorCelsius + " Kelvin = " + valorKelvin;
	}
	
}
